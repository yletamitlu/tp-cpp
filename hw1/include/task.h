#ifndef TP_CPP_ALIGNMENT_H
#define TP_CPP_ALIGNMENT_H

#include <stdlib.h>

#include "tools.h"

int** align_vectors(int **vectors, int count);

#endif //TP_CPP_ALIGNMENT_H
