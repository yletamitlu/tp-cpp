#ifndef TP_CPP_TOOLS_H
#define TP_CPP_TOOLS_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define BUFFER_SIZE 64

int find_max_size(const int* sizes, int count);

char* gets_s(char* ptr, size_t size, FILE* stream);

bool is_digit_string(const char* str);

#endif //TP_CPP_TOOLS_H
