#ifndef TP_CPP_VECTORS_H
#define TP_CPP_VECTORS_H

#include <stdio.h>
#include <stdlib.h>

#include "tools.h"

void print_vectors(int** vectors, int count);

int** write_vectors(FILE* stream, int count);

void free_vectors(int** vectors, int count);

#endif //TP_CPP_VECTORS_H
