#include "task.h"
#include "vectors.h"

int main(int argc, const char* argv[]) {
    FILE* stream = stdin;

    if (argc >= 2 && strcmp(argv[1], "--file") == 0) {
        stream = fopen(argv[2], "r");
    }

    if (stream == NULL) {
        return -1;
    }

    int count = 0;

    if (stream == stdin) {
        printf("Введите количество векторов: ");
        scanf("%i", &count);
        getchar();
    } else if (fscanf(stream, "%d", &count) == 1) {
        // scanf сдвигает каретку на одну позицию,
        // если не переместить ее с указания количества векторов на следующую позицию,
        // то далее fgets считает пустую строку от текущей позиции каретки
        fseek(stream, 1, SEEK_CUR);
    } else {
        return -1;
    }

    if (count <= 0) {
        return -1;
    }

    int** vectors = write_vectors(stream, count);

    if (vectors == NULL) {
        return -1;
    }

    int** result = align_vectors(vectors, count);

    print_vectors(result, count);

    free_vectors(vectors, count + 1); // +1 sizes pointer

    if (stream != stdin) {
        fclose(stream);
    }

    return 0;
}
