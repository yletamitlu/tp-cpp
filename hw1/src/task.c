#include "task.h"

int** align_vectors(int **vectors, int count) {
    int* sizes = vectors[count];
    int max_size = find_max_size(sizes, count);

    for (int i = 0; i < count; i++) {
        vectors[i] = realloc(vectors[i], sizeof(int*) * max_size);
        for (int j = sizes[i]; j < max_size; j++) {
            vectors[i][j] = 0;
        }
    }

    return vectors;
}
