#include "../include/tools.h"

int find_max_size(const int* sizes, int count) {
    if (!sizes) {
        return -1;
    }

    int max = sizes[0];
    for (int i = 1; i < count; i++) {
        if (sizes[i] > max) {
            max = sizes[i];
        }
    }

    return max;
}

char* gets_s(char* ptr, size_t size, FILE* stream) {
    if (ptr == NULL || stream == NULL) {
        return NULL;
    }

    if (fgets(ptr, size, stream) == NULL) {
        return NULL;
    }

    if (ptr[strlen(ptr) - 1] == '\n') {
        ptr[strlen(ptr) - 1] = '\0';
    }

    return ptr;
}

bool is_digit_string(const char* str) {
    size_t size = strlen(str);
    bool is_valid = true;

    for (int i = 0; i < size && is_valid; i++) {
        if (!((str[i] >= '0' && str[i] <= '9') || str[i] == ' ' || str[i] == '-')) {
            is_valid = false;
        }
    }

    return is_valid;
}
