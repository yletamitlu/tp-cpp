#include "../include/vectors.h"

void print_vectors(int** vectors, int count) {
    if (!vectors) {
        return;
    }

    for (int i = 0; i < count; i++) {
        for (int j = 0; j < find_max_size(vectors[count], count); j++) {
            if (!vectors[i]) {
                return;
            }
            printf("%i ", vectors[i][j]);
        }
        printf("\n");
    }
}

int** write_vectors(FILE* stream, int count) {
    if (count <= 0 || !stream) {
        return NULL;
    }

    int** vectors = malloc(sizeof(int*) * (count + 1)); // размеры векторов лежат в последнем векторе

    if (!vectors) {
        return NULL;
    }

    int* sizes = malloc(sizeof(int) * count);

    if (!sizes) {
        free(vectors);
        return NULL;
    }

    char buffer[BUFFER_SIZE];

    for (int i = 0; i < count; i++) {
        if (gets_s(buffer, sizeof(buffer), stream) == NULL) {
            return NULL;
        }

        if (!is_digit_string(buffer)) {
            return NULL;
        }

        vectors[i] = malloc(sizeof(int));

        if (!vectors[i]) {
            return NULL;
        }

        int j = 0;

        char* end = NULL;
        vectors[i][j++] = strtol(buffer, &end, 10);

        while (*end != '\0') {
            vectors[i][j] = strtol(end + 1, &end, 10);
            vectors[i] = realloc(vectors[i], sizeof(int*) * j++);
        }

        sizes[i] = j;
    }

    vectors[count] = sizes;

    return vectors;
}

void free_vectors(int** vectors, int count) {
    if (!vectors) {
        return;
    }

    for (int i = 0; i < count; i++) {
        free(vectors[i]);
    }
    free(vectors);
}
