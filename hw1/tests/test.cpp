#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <string.h>
#include <fstream>

#include "test.h"

extern "C" {
#include "../include/tools.h"
#include "task.h"
}

// ============ TEST find_max_size() =============== //

TEST(TestFindMaxSize, Test1) {
    int size = 3;
    int array[3] = {3, 11, 7};

    int result = find_max_size(array, size);

    EXPECT_EQ(11, result);
}

TEST(TestFindMaxSize, Test2) {
    int size = 6;
    int array[6] = {0, 0, 0, 0, 0, 0};

    int result = find_max_size(array, size);

    EXPECT_EQ(0, result);
}

TEST(TestFindMaxSize, Test3) {
    int size = 7;
    int array[7] = {1, 2, 3, 23, 9, 8, 23};

    int result = find_max_size(array, size);

    EXPECT_EQ(23, result);
}

// ============ TEST gets_s() =============== //

TEST(TestGetsS, TestOK) {
    const std::string path = TEST_DATA_PATH + std::string("gets_s_data.txt");

    FILE* f = fopen(path.c_str(), "r");

    char buffer[BUFFER_SIZE];

    char* ptr = gets_s(buffer, BUFFER_SIZE, f);

    EXPECT_STREQ(ptr, "5 1 1 9 14");

    fclose(f);
}

TEST(TestGetsS, TestPtrIsNull) {
    const std::string path = TEST_DATA_PATH + std::string("gets_s_data.txt");

    FILE* f = fopen(path.c_str(), "r");

    char* ptr = gets_s(nullptr, BUFFER_SIZE, f);

    EXPECT_EQ(ptr, nullptr);

    fclose(f);
}

TEST(TestGetsS, TestStreamIsNull) {
    char buffer[BUFFER_SIZE];

    char* ptr = gets_s(buffer, BUFFER_SIZE, nullptr);

    EXPECT_EQ(ptr, nullptr);
}

// ============ TEST is_digit_string() =============== //

TEST(TestIsDigitString, TestStringHasOnlyDigits) {
    char* str = (char*)malloc(sizeof(char) * 12);
    for (int i = 0; i < 12; i++) {
        str[i] = i;
    }

    EXPECT_EQ(is_digit_string(str), true);
    free(str);
}

TEST(TestIsDigitString, TestStringHasOnlyDigits1) {
    char buffer[9] = "10 82 -3";
    char* str = buffer;

    EXPECT_EQ(is_digit_string(str), true);
}

TEST(TestIsDigitString, TestStringHasOnlyDigits2) {
    char buffer[4] = "1 K";
    char* str = buffer;

    EXPECT_EQ(is_digit_string(str), false);
}

// ============ TEST align_vectors() =============== //

void readTestData(std::ifstream& f, std::vector<int*>& vectors, int* sizesVector) {
    int size = vectors.capacity();

    for (int i = 0; i < size; i++) {
        std::string str;
        getline(f, str);

        int j = 0;
        std::stringstream sstream(str);
        while (!sstream.eof()) {
            sstream >> vectors[i][j++];
        }

        if (sizesVector != nullptr) {
            sizesVector[i] = j;
        }
    }
}

TEST(TestAlignment, Test1) {
    std::ifstream f1, f2;

    f1.open(TEST_DATA_PATH + std::string("align_test_1.txt"));
    f2.open(TEST_DATA_PATH + std::string("align_expected_1.txt"));

    int count = 0;
    f1 >> count;

    f1.seekg(2); f2.seekg(2);

    std::vector<int*> vectors(count), expected(count);

    for (int i = 0; i < vectors.capacity(); i++) {
        vectors[i] = (int*)malloc(sizeof(int) * MAX_VECTOR_SIZE);
        expected[i] = (int*)malloc(sizeof(int) * MAX_VECTOR_SIZE);
    }

    int* sizes = (int*)malloc(sizeof(int) * count);

    readTestData(f1, vectors, sizes);
    readTestData(f2, expected, nullptr);

    vectors.push_back(sizes);

    int columns = find_max_size(sizes, count);

    int** result = align_vectors(vectors.data(), count);

    for (int i = 0; i < count ; i++) {
        for (int j = 0; j < columns; j++) {
            EXPECT_EQ(result[i][j], expected[i][j]);
        }
    }

    for (int i = 0; i < count; i++) {
        free(vectors[i]);
        free(expected[i]);
    }

    free(sizes);

    f1.close(); f2.close();
}

TEST(TestAlignment, Test2) {
    std::ifstream f1, f2;

    f1.open(TEST_DATA_PATH + std::string("align_test_2.txt"));
    f2.open(TEST_DATA_PATH + std::string("align_expected_2.txt"));

    int count = 0;
    f1 >> count;

    f1.seekg(2); f2.seekg(2);

    std::vector<int*> vectors(count), expected(count);

    for (int i = 0; i < vectors.capacity(); i++) {
        vectors[i] = (int*)malloc(sizeof(int) * MAX_VECTOR_SIZE);
        expected[i] = (int*)malloc(sizeof(int) * MAX_VECTOR_SIZE);
    }

    int* sizes = (int*)malloc(sizeof(int) * count);

    readTestData(f1, vectors, sizes);
    readTestData(f2, expected, nullptr);

    vectors.push_back(sizes);

    int columns = find_max_size(sizes, count);

    int** result = align_vectors(vectors.data(), count);

    for (int i = 0; i < count ; i++) {
        for (int j = 0; j < columns; j++) {
            EXPECT_EQ(result[i][j], expected[i][j]);
        }
    }

    for (int i = 0; i < count; i++) {
        free(vectors[i]);
        free(expected[i]);
    }

    free(sizes);

    f1.close(); f2.close();
}

TEST(TestAlignment, Test3) {
    std::ifstream f1, f2;

    f1.open(TEST_DATA_PATH + std::string("align_test_3.txt"));
    f2.open(TEST_DATA_PATH + std::string("align_expected_3.txt"));

    int count = 0;
    f1 >> count;

    f1.seekg(2); f2.seekg(2);

    std::vector<int*> vectors(count), expected(count);

    for (int i = 0; i < vectors.capacity(); i++) {
        vectors[i] = (int*)malloc(sizeof(int) * MAX_VECTOR_SIZE);
        expected[i] = (int*)malloc(sizeof(int) * MAX_VECTOR_SIZE);
    }

    int* sizes = (int*)malloc(sizeof(int) * count);

    readTestData(f1, vectors, sizes);
    readTestData(f2, expected, nullptr);

    vectors.push_back(sizes);

    int columns = find_max_size(sizes, count);

    int** result = align_vectors(vectors.data(), count);

    for (int i = 0; i < count ; i++) {
        for (int j = 0; j < columns; j++) {
            EXPECT_EQ(result[i][j], expected[i][j]);
        }
    }

    for (int i = 0; i < count; i++) {
        free(vectors[i]);
        free(expected[i]);
    }

    free(sizes);

    f1.close(); f2.close();
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
